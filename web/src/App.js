import React, { useState } from 'react'
import axios from 'axios'
import CanvasJSReact from './canvasjs.react'
import './App.css'

const ANY_URL = /^((http(s)?:\/\/))+[(www.)?a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)$/
const FIVE_MINUTES = 10 * 60 * 1000
const SHORT_URL = /^((http(s)?:\/\/))*(ursh.herokuapp.com\/)([-a-zA-Z0-9]{12})$/

const CanvasJSChart = CanvasJSReact.CanvasJSChart

const dateData = data => {
	const date = new Date(data.date)
	const dateNow = Date.now()
	let lastHour = []
	const ONE_HOUR = 60 * 60 * 1000
	const visits = []

	for (let index in data.visits) {
		const visit = data.visits[index]
		const visitDate = new Date(visit)
		visits.push(visitDate)
		if (dateNow - visit < ONE_HOUR) lastHour.push(visit)
	}

	const baseTime = dateNow - ONE_HOUR

	const options = {
		animationEnabled: true,
		axisX: {
			valueFormatString: 'HH:mm',
		},
		data: [
			{
				yValueFormatString: '#',
				xValueFormatString: 'HH:mm',
				type: 'spline',
				dataPoints: [],
			},
		],
	}

	for (let index = 1; index < 7; index++) {
		const cur = baseTime + index * FIVE_MINUTES
		const prev = baseTime + (index - 1) * FIVE_MINUTES
		let x = new Date(cur)
		const y = lastHour.filter(visit => visit > prev && visit < cur).length
		options.data[0].dataPoints.push({ x, y })
	}

	return { ...data, date: date.toString(), visits, lastHour, options }
}

const App = () => {
	const [options, setOptions] = useState(null)

	const [response, setResponse] = useState(null)

	const [url, setUrl] = useState('')

	const submitHandler = event => {
		event.preventDefault()
		if (SHORT_URL.test(url))
			axios
				.get('https://ursh.herokuapp.com/v1/' + url.split('/').reverse()[0])
				.then(response => {
					const data = dateData(response.data)
					setResponse(data)
					setOptions(data.options)
				})
				.catch(error => console.log(error))
		else if (ANY_URL.test(url))
			axios
				.post('https://ursh.herokuapp.com/v1', { to: url })
				.then(response => {
					const data = dateData(response.data)
					setResponse(data)
					setOptions(data.options)
				})
				.catch(error => console.log(error))
		else {
			alert('Invalid URL')
			setResponse(null)
		}
	}

	const urlHandler = ({ target: { value } }) => setUrl(value)

	return (
		<div className='App'>
			<form className='Form' onSubmit={submitHandler}>
				<h1>URL Shortener</h1>
				<input
					autoFocus
					className='Input'
					onChange={urlHandler}
					placeholder='Enter A URL'
					value={url}
				/>
				<input className='Button' type='submit' value='Go' />
			</form>
			<div className='AppResponse'>
				{response && (
					<>
						<p>
							<b>Shortened Link</b>:{' '}
							<a
								href={'https://ursh.herokuapp.com/' + response.from}
								rel='noopener noreferrer'
								target='_blank'
							>
								https://ursh.herokuapp.com/{response.from}
							</a>
						</p>
						<p>
							<b>Redirect To</b>:{' '}
							<a href={response.to} rel='noopener noreferrer' target='_blank'>
								{response.to}
							</a>
						</p>
						<p>
							<b>Creation Date-Time</b>: {response.date}
						</p>
						<p>
							<b>Total Visits</b>: {response.visits.length}
						</p>
						<p>
							<b>Visits in the last hour</b>:
						</p>
						<CanvasJSChart options={options} />
					</>
				)}
				{!response && (
					<p>
						Enter a URL to get shortened URL or enter shortened URL to get info
						on the URL.
					</p>
				)}
			</div>
		</div>
	)
}

export default App
