/*
 * api for manipulation of URLs
 *
 * get /:url - get info on a URL
 * post / - create new URL
 *
 */

const { Router } = require('express')

const URLModel = require('./model')
const validator = require('./validator')(false)
const internalError = require('../../utils/internalError')

// function to generate a string with random characters
const genString = length => {
	let result = ''
	let characters =
		'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
	let charactersLength = characters.length
	for (let i = 0; i < length; i++)
		result += characters.charAt(Math.floor(Math.random() * charactersLength))
	return result
}

const router = Router()

// get url if it exists
router.get('/:url', async (req, res) => {
	try {
		const URL = await URLModel.findOne({ from: req.params.url })
		if (!URL) return res.status(401).json({ msg: 'URL does not exist' })
		return res.json(URL)
	} catch (err) {
		internalError(err, res)
	}
})

// create new URL
router.post('/', validator, async (req, res) => {
	try {
		let from = ''
		const to = req.body.to
		let checkDuplicate = true
		while (checkDuplicate) {
			from = genString(12)
			checkDuplicate = await URLModel.findOne({ from })
		}
		const URL = new URLModel({ from, to })
		const NewURL = await URL.save()

		res.json({ msg: 'URL created', NewURL })
	} catch (err) {
		internalError(err, res)
	}
})

module.exports = router
