// schema for URLs

module.exports = {
	// creation date time
	date: {
		default: Date.now(),
		required: true,
		type: Number,
	},
	// shortend URL
	from: {
		required: true,
		type: String,
	},
	// destination of URL
	to: {
		required: true,
		type: String,
	},
	// page visit records
	visits: {
		default: [],
		required: false,
		type: Array,
	},
}
