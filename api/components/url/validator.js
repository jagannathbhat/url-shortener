// validation for URLs

// contains display names and validation methods for fields in the request
const Fields = {
	date: {
		name: 'date',
		// Check if to contains a 13 digit number
		validation: value => {
			const patt = /^[0-9]{13}$/
			return patt.test(value)
		},
	},
	from: {
		name: 'from',
		// check if type of the value is a string
		validation: value => typeof value === 'string',
	},
	to: {
		name: 'to',
		// Check if to contains a proper http(s) url
		validation: value => {
			const patt = /^((http(s)?:\/\/))*[(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$/
			return patt.test(value)
		},
	},
	visits: {
		name: 'visits',
		validation: value => true,
	},
}

const Schema = require('./schema')

module.exports = (strict = true) => (req, res, next) =>
	require('../../utils/validator')(Fields, req, res, Schema, strict, next)
