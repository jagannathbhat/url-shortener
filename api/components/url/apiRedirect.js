/*
 * api performing redirection
 *
 * get /:url - redirect to URL
 *
 */

const { Router } = require('express')

const URLModel = require('./model')
const internalError = require('../../utils/internalError')

const router = Router()

// redirect to URL
router.get('/:url', async (req, res) => {
	try {
		const URL = await URLModel.findOne({ from: req.params.url })
		if (!URL) return res.status(401).json({ msg: 'URL does not exist' })
		URL.visits.push(Date.now())
		await URL.save()
		return res.redirect(URL.to)
	} catch (err) {
		internalError(err, res)
	}
})

module.exports = router
