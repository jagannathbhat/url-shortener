// mongoose model for URLs

const { Schema, model } = require('mongoose')

const URLSchema = Schema(require('./schema'))

module.exports = model('url', URLSchema)
