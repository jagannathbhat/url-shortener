// express server that handles the api requests

// load environment variables from .env file
require('dotenv').config()
const cors = require('cors')
const express = require('express')

const app = express()
const { json } = express
// use port specified in environment or use port 5000
const PORT = process.env.PORT || 5000

// connect to database
require('./utils/db')()

// enable cross origin requests
app.use(cors())

// decode application/json requests
app.use(json({ extended: false }))

// api v1 route
app.use('/v1', require('./routes/v1'))
app.use(require('./components/url/apiRedirect'))

// start server on port
module.exports = app.listen(PORT, () =>
	console.log(`server started on ${PORT}`)
)
