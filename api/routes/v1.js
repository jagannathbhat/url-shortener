// api v1 routes

const { Router } = require('express')

const router = Router()

router.use(require('../components/url/api'))

module.exports = router
