// function to respond with server error

module.exports = (err, res, msg = 'server error') => {
	console.error(err)
	res.status(500).send({ msg })
}
