// function to perform validation on requests

module.exports = (Fields, req, res, Schema, strict, next) => {
	// check if unwanted fields are not included in the body
	const bodyKeys = Object.keys(req.body).sort()
	const schemaKeys = Object.keys(Schema).sort()
	const bodyKeysCheck = bodyKeys.filter(key => !schemaKeys.includes(key))
	if (bodyKeysCheck.length > 0)
		return res.status(401).json({ msg: 'Invalid request' })

	// array to store all validation errors
	const errors = []

	for (let field in Schema) {
		const value = req.body[field]
		const { name, validation } = Fields[field]
		const { required } = Schema[field]

		// check if required field is present, provided strict is set to true
		if (strict && required && !value) {
			errors.push(name + ' is required')
			continue
		}

		// perform validation on the value
		if (value && validation && !validation(value))
			errors.push('Invalid ' + name)
	}

	// respond with errors if they exist
	if (errors.length > 0) return res.status(401).json({ msg: errors })

	// else continue
	next()
}
