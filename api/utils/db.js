// function to connect to database

const { connect } = require('mongoose')

// get uri of database from environment variable
const db = process.env.MONGO_URI

module.exports = async () => {
	try {
		await connect(db, {
			useNewUrlParser: true,
			useCreateIndex: true,
			useFindAndModify: false,
			useUnifiedTopology: true,
		})
		console.log('MongoDB Connected')
	} catch (err) {
		console.error(err.message)
		process.exit(1)
	}
}
